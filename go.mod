module gitlab.com/trbroyles1/goconfig

go 1.14

require (
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	gitlab.com/trbroyles1/golog v0.2.3
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.2.8
)
