/*
Copyright 2020 Ryan Broyles (https://trbroyles.com/).
Released under the terms of the Modified BSD License
https://opensource.org/licenses/BSD-3-Clause
*/

package config

import (
	"encoding/json"
	"fmt"
	"os"
	"sort"

	logging "gitlab.com/trbroyles1/golog"
	"gopkg.in/yaml.v2"
)

type StorageFormat int

const (
	StorageFormatJSON StorageFormat = iota
	StorageFormatYAML StorageFormat = iota
)

type decoder interface {
	Decode(interface{}) error
}

type encoder interface {
	Encode(interface{}) error
}

type VersionSpec struct {
	VersionNumber float32
	Applyer       func()
}

type BasicConfig struct {
	ConfigVersion float32
	LogLevel      logging.LogLevel
}

func (c *BasicConfig) GetCurrentVersion() float32 {
	return c.ConfigVersion
}

func (c *BasicConfig) SetCurrentVersion(v float32) {
	c.ConfigVersion = v
}

type ConfigSpecifier interface {
	GetCurrentVersion() float32
	SetCurrentVersion(float32)
	GetVersionMatrix() []VersionSpec
}

type ConfigManager struct {
	logger        logging.Logger
	configSpec    ConfigSpecifier
	storageFormat StorageFormat
}

func NewConfigManager(spec ConfigSpecifier, logger logging.Logger, sf StorageFormat) *ConfigManager {
	cm := new(ConfigManager)
	cm.logger = logger
	cm.configSpec = spec
	cm.storageFormat = sf
	return cm
}

func (me *ConfigManager) LoadConfig() {
	err := me.loadConfigFromFile()
	if err != nil {
		me.logger.Err(err.Error())
		switch me.storageFormat {
		case StorageFormatJSON:
			me.logger.Init("Making default configuration in config.defaults.json. Please update it as needed and save as config.json, then re-run")
			me.applyConfigUpdates()
			err = me.saveConfigFile("config.defaults.json")
		case StorageFormatYAML:
			me.logger.Init("Making default configuration in config.defaults.yaml. Please update it as needed and save as config.json, then re-run")
			me.applyConfigUpdates()
			err = me.saveConfigFile("config.defaults.yaml")
		}
		if err != nil {
			me.logger.Err(err.Error())
		}
		os.Exit(1)
	}

	if countAppliedUpdates := me.applyConfigUpdates(); countAppliedUpdates > 0 {
		me.logger.Init(fmt.Sprintf("%d update(s) applied", countAppliedUpdates))
		me.logger.Init("Saving updated config file. Please verify / update it as needed, then re-run")
		if err = me.saveConfigFile(me.configFileName()); err != nil {
			me.logger.Err(err.Error())
		}
		os.Exit(1)
	}
}

func (me *ConfigManager) loadConfigFromFile() error {
	var file *os.File
	file, err := os.Open(me.configFileName())
	if err != nil {
		return err
	}
	defer file.Close()

	var d decoder
	switch me.storageFormat {
	case StorageFormatJSON:
		d = json.NewDecoder(file)
	case StorageFormatYAML:
		d = yaml.NewDecoder(file)
	}

	err = d.Decode(me.configSpec)
	return err
}

func (me *ConfigManager) applyConfigUpdates() int {
	sortedVersionSpecs := me.configSpec.GetVersionMatrix()
	sort.Slice(sortedVersionSpecs, func(i, j int) bool { return sortedVersionSpecs[i].VersionNumber < sortedVersionSpecs[j].VersionNumber })
	countAppliedUpdates := 0
	for _, vs := range sortedVersionSpecs {
		if vs.VersionNumber > me.configSpec.GetCurrentVersion() {
			me.logger.Init(fmt.Sprintf("Applying config version %.2f", vs.VersionNumber))
			vs.Applyer()
			me.configSpec.SetCurrentVersion(vs.VersionNumber)
			countAppliedUpdates++
		}
	}
	return countAppliedUpdates
}

func (me *ConfigManager) saveConfigFile(fileName string) error {
	file, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	var e encoder
	switch me.storageFormat {
	case StorageFormatJSON:
		enc := json.NewEncoder(file)
		enc.SetIndent("", "\t")
		e = enc
	case StorageFormatYAML:
		enc := yaml.NewEncoder(file)
		defer enc.Close()
		e = enc
	}

	err = e.Encode(me.configSpec)
	return err
}

func (me *ConfigManager) SaveConfig() {
	me.saveConfigFile(me.configFileName())
}

func (me *ConfigManager) configFileName() string {
	if me.storageFormat == StorageFormatJSON {
		return "config.json"
	}
	return "config.yaml"
}
